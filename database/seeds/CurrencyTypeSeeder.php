<?php

use Illuminate\Database\Seeder;
use App\Models\Master\CurrencyType;

class CurrencyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type1 = CurrencyType::create(['code'=> 'IDR', 'name'=> 'Rupiah', 'country'=> 'Indonesia']);
        $type2 = CurrencyType::create(['code'=> 'USD', 'name'=> 'US Dollar', 'country'=> 'USA']);
    }
}
