<?php

use Illuminate\Database\Seeder;
use App\Models\Master\EmployeeType;

class EmployeeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type1 = EmployeeType::create(['code'=> '01', 'name'=> 'Management']);
        $type2 = EmployeeType::create(['code'=> '02', 'name'=> 'Employee']);
    }
}
