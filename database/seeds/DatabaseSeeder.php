<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(EmployeeTypeSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(CurrencyTypeSeeder::class);

        // $this->call(UnitTableSeeder::class);
    }
}
