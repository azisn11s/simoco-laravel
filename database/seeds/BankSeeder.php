<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bank1 = Bank::create(['code'=> 'BRI', 'name'=> 'Bank Rakyat Indonesia']);
        $bank2 = Bank::create(['code'=> 'BNI', 'name'=> 'Bank Negara Indonesia']);
    }
}
