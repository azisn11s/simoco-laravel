<?php

use App\Models\Master\Company;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company1 = Company::create(['code'=> '01', 'name'=> 'PT. SIMOCO INDONESIA']);
        $company2 = Company::create(['code'=> '02', 'name'=> 'PT. MOBILE DIGITAL NETWORK INDONESIA']);
    }
}
