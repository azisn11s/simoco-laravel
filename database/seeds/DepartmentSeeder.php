<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department1 = Department::create(['code'=> 'DFA', 'name'=> 'Finance & Accounting', 'description'=> 'Finance Division']);
        $department2 = Department::create(['code'=> 'DLP', 'name'=> 'Logistics & Purchasing', 'description'=> 'Logistic Division']);

    }
}
