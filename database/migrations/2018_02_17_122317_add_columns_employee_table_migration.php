<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsEmployeeTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_employee', function (Blueprint $table) {
            $table->integer('employee_type_id')->unsigned();
            $table->string('place_of_birth', 40)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('address')->nullable();
            $table->string('home_phone', 20)->nullable();
            $table->string('mobile_phone', 20)->nullable();
            $table->date('start_work_date')->nullable();
            $table->enum('status', ['Active', 'InActive'])->default('Active');

            $table->foreign('employee_type_id')
                ->references('id')
                ->on('mst_employee_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_employee', function (Blueprint $table) {
            
            $table->dropForeign('mst_employee_employee_type_id_foreign');

            $table->dropColumn('employee_type_id');
            $table->dropColumn('place_of_birth');
            $table->dropColumn('date_of_birth');
            $table->dropColumn('address');
            $table->dropColumn('home_phone');
            $table->dropColumn('mobile_phone');
            $table->dropColumn('start_work_date');
            $table->dropColumn('status');

        });
    }
}
