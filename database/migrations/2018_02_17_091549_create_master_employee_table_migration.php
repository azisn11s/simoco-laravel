<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterEmployeeTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_employee', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code', 10)->unique();
            $table->string('first_name', 50);
            $table->string('last_name', 70)->nullable();
            $table->integer('department_id')->unsigned();
            $table->string('email_1', 40);
            $table->string('email_2', 40)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_employee');
    }
}
