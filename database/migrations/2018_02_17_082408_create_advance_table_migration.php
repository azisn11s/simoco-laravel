<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_advance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id')->unsigned();
            $table->bigInteger('supplier_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->bigInteger('project_id')->unsigned();
            $table->date('request_date');
            $table->integer('payment_type_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->string('purpose', 150)->nullable();
            $table->integer('total_idr_request')->nullable();
            $table->integer('total_currency_request')->nullable();
            $table->date('plan_date_on')->nullable();
            $table->date('plan_date_out')->nullable();
            $table->integer('total_manpower');
            $table->integer('element_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_advance');
    }
}
