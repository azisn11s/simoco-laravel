<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdvanceCompanyRelationshipMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            
            $table->foreign('company_id')
                ->references('id')
                ->on('mst_company')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            
            $table->dropForeign('trx_advance_company_id_foreign');

        });
    }
}
