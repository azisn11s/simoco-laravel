<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipTrxStepProjectPaymentTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_step_project_payment', function (Blueprint $table) {
            $table->foreign('project_id')
                ->references('id')
                ->on('mst_project')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('currency_type_id')
                ->references('id')
                ->on('mst_currency_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_step_project_payment', function (Blueprint $table) {
            $table->dropForeign('trx_step_project_payment_project_id_foreign');
            $table->dropForeign('trx_step_project_payment_currency_type_id_foreign');
        });
    }
}
