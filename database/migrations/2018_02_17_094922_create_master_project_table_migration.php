<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterProjectTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('project_number', 10)->unique();
            $table->char('customer_code', 10)->nullable();
            $table->string('originator', 70)->nullable();
            $table->string('contributor_number', 70)->nullable();
            $table->date('project_date');
            $table->integer('currency_type_id')->unsigned();
            $table->double('value');
            $table->integer('element_id')->unsigned()->nullable();
            $table->date('log_date')->nullable();
            $table->date('equip_rec_date')->nullable();
            $table->char('code_inv', 2)->nullable();
            $table->date('inv_date')->nullable();
            $table->string('description')->nullable();
            $table->date('target_date')->nullable();
            $table->char('status_code', 1)->nullable();
            $table->date('closed_date')->nullable();
            $table->char('approve_code', 2)->nullable();
            $table->dateTime('approve_date')->nullable();
            $table->integer('entry_by')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->string('customer_name')->nullable();
            $table->string('po_number')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->string('finance_progress', 50)->nullable();
            $table->integer('total_budget_idr')->nullable();
            $table->double('total_budget_currency')->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_project');
    }
}
