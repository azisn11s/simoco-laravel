<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdvanceSupplierRelationshipMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            $table->foreign('supplier_id')
                ->references('id')
                ->on('mst_supplier')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            $table->dropForeign('trx_advance_supplier_id_foreign');
        });
    }
}
