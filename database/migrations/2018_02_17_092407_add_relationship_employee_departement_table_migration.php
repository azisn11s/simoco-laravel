<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipEmployeeDepartementTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_employee', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')
                ->on('mst_department')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_employee', function (Blueprint $table) {
            $table->dropForeign('mst_employee_department_id_foreign');
        });
    }
}
