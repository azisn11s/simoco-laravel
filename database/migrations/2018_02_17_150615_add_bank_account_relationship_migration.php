<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountRelationshipMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_bank_account', function (Blueprint $table) {
            
            $table->foreign('bank_id')
                ->references('id')
                ->on('mst_bank')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('currency_type_id')
                ->references('id')
                ->on('mst_currency_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_bank_account', function (Blueprint $table) {
            
            $table->dropForeign('mst_bank_account_bank_id_foreign');
            $table->dropForeign('mst_bank_account_currency_type_id_foreign');

        });
    }
}
