<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrxAdvanceRelationshipMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            $table->foreign('employee_id')
                ->references('id')
                ->on('mst_employee')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('department_id')
                ->references('id')
                ->on('mst_department')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('mst_project')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('payment_type_id')
                ->references('id')
                ->on('mst_payment_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('payment_method_id')
                ->references('id')
                ->on('mst_payment_method')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('element_id')
                ->references('id')
                ->on('mst_element')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_advance', function (Blueprint $table) {
            $table->dropForeign('trx_advance_employee_id_foreign');
            $table->dropForeign('trx_advance_department_id_foreign');
            $table->dropForeign('trx_advance_project_id_foreign');
            $table->dropForeign('trx_advance_payment_type_id_foreign');
            $table->dropForeign('trx_advance_payment_method_id_foreign');
            $table->dropForeign('trx_advance_element_id_foreign');
        });
    }
}
