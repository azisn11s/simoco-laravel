<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterSupplierTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_supplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->char('code')->unique()->nullable();
            $table->string('address')->nullable();
            $table->string('contact_name', 70)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->enum('type', ['L', 'N'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_supplier');
    }
}
