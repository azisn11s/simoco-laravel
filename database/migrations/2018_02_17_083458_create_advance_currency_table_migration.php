<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceCurrencyTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_advance_currency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('advance_id')->unsigned();
            $table->integer('currency_type_id')->unsigned();
            $table->string('name', 100);
            $table->integer('total');
            $table->timestamps();

            $table->foreign('advance_id')->references('id')->on('trx_advance')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_advance_currency', function (Blueprint $table) {
            $table->dropForeign('trx_advance_currency_advance_id_foreign');
        });
        
        Schema::dropIfExists('trx_advance_currency');
    }
}
