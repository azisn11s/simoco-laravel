function toDate1(date) {
    var sqlDate = "";

    var dateString = String(date);

    var day = dateString.substr(8, 2);
    var month = dateString.substr(5, 2);
    var year = dateString.substr(0, 4);

    sqlDate = day + "-" + month + "-" + year;

    return sqlDate;
};

function phoneToSubmit (phone_number) {
	var desired = phone_number.replace(/[-_\s]/g, '');
	return desired;
};

window.fadeIn = function(obj) {
    $(obj).fadeIn(1000);
};

//Scroll Up and Focus
function scrolldiv(divName){
  $('html, body').animate({
      scrollTop: $("#"+ divName).offset().top
  }, 1000);
};

//Get Dropdown Items
function getDropdownItems(type){
    var data = ($.ajax({
        url:  encodeURI("../general/general/binding_array/"+type),
        dataType: "json", 
        async: false,
    }).responseText);

    return $.parseJSON(data);
};

$(document).ready(function(){



    //Prevent submitting form by enter key
    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        alert("Please click Submit Button for processing this Form!")
        return false;
      }
    });

    //Datemask dd/mm/yyyy
    $(".date-mask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

    //Date picker
    $('.date').datepicker({
      autoclose: true
    });

    //Loading state master


    //Set Money Format on itemTemplate Grid Properties
    Number.prototype.setMoneyformat = function(n, x, s, c) {
      var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
          num = this.toFixed(Math.max(0, ~~n));

      return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    //Data Mask
    $("[data-mask]").inputmask();

    //Custom Field | Grid | moneyField
    var MoneyField = function(config) {
        jsGrid.Field.call(this, config);
    };
    MoneyField.prototype = new jsGrid.Field({
        itemTemplate: function(value) {
          var numb = new Number(value);
            return numb.setMoneyformat(2, 3, '.', ',');
          // return this._item = $(this).val(value).autoNumeric('init', {aSep: '.', aDec: ',' });
        },
        insertTemplate: function(value) {
            return this._insertMoney = $("<input>").autoNumeric('init', {aSep: '.', aDec: ',' });
        },
        editTemplate: function(value) {
            // return this._editMoney = $("<input>").autoNumeric('set', value);
            // $(selector).autoNumeric('destroy');
            return this._editMoney = $("<input>").val('').autoNumeric('init', {aSep: '.', aDec: ',' }).autoNumeric('set', value);
        },
        insertValue: function() {
            // return this._insertMoney.val();
            return this._insertMoney.autoNumeric('get');
            // alert(this._insertMoney.val());
        },
        editValue: function() {
            // return this._editMoney.val();
            return this._editMoney.autoNumeric('get');
        }
    });
    jsGrid.fields.moneyField = MoneyField;


    //Custom Field | Grid | myDateField
    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };
 
    MyDateField.prototype = new jsGrid.Field({
        /*
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
        */
 
        itemTemplate: function(value) {
            return value;
        },
 
        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datepicker({ format: 'dd/mm/yyyy' });   
        },

        insertValue: function() {
            var dateTime = this._insertPicker.val();
            
            return dateTime;
        },
 
        editTemplate: function(value) {
          if(value){
            return this._editPicker = $("<input>").datepicker({ format: 'dd/mm/yyyy' }).val(value);
          }
            return this._editPicker = $("<input>").datepicker({ format: 'dd/mm/yyyy' });
        },
 
        editValue: function() {
            var dateTime = this._editPicker.val();
            return dateTime;
        }
    });
 
    jsGrid.fields.myDateField = MyDateField;

});