

$(document).ready(function (){

    /* ON RESEARCH
    * EMPLOYEE LIST
    *
    * Using global function JS
    * Using General Controller
    */
    var itemDept = getDropdownItems("division");
                  
    $('#grid_emp').jsGrid({
        width: "100%",
        height: "auto",
        inserting: true,
        editing: true,
        filtering: false,
        sorting: true,
        paging: true,
        // pageLoading: true,
        pageSize: 10,
        modeSwitchButton: false,
        autoload: true,

        controller: {
            loadData: function() {
                var deferred = $.Deferred();

                $.ajax({
                    url: encodeURI("../admin/employee/get_emp_list"),
                    dataType: "json",
                }).done(function(response){
                    deferred.resolve(response);
                });	 

                return deferred.promise();

            },				             
			insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: encodeURI("../admin/employee/save"),
                    data: item
                });    
            },
             
            updateItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: encodeURI("../admin/employee/save/"+item.emp_id),
                    data: item
                }); 
            },
            
            deleteItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: encodeURI("../admin/employee/remove/"+item.emp_id),
                });
            }
        },

        fields: [
        	{ name: "emp_id", type: "text", visible:false },
            { name: "emp_code", type: "text", title: "Emp. Code", width: 50 },
            { name: "emp_first_name", type: "text", width: 80, title: "First Name", },
            { name: "emp_last_name", type: "text", width: 100, title: "Last Name", },
            { 
                name: "emp_div_id", type: "select",  title: "Department", sorting: false, items: itemDept, 
                valueField: "id", textField: "name", valueType: "number|string"
            },
            { name: "emp_email1", type: "text", width: 100, title: "Email (1)", },
            { name: "emp_email2", type: "text", width: 100, title: "Email (2)", },
            { type: "control" }
        ]
    });


});