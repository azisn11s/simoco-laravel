@extends('base.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employee Type
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
    <li><a href="{{ route('employee-types.index') }}"></i> Employee Type</a></li>
    <li class="active">Add Record</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Employee Type</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6">
                <!-- Horizontal Form -->
                <!-- form start -->
                {{-- <form class="form-horizontal" action="{{ route('companies.update') }}" method="put"> --}}
                {!! Form::model($employee_type, ['url' => route('employee-types.update', $employee_type->id), 'method' => 'put', 'class'=>'form-horizontal']) !!}
                  @include('employee-types._form')
                 {!! Form::close() !!}
                {{-- </form> --}}
                <!-- form end -->
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
</section>
<!-- /.content -->
@endsection


