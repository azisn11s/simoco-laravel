@extends('base.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Banks
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
    <li class="active">Banks</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Companies</h3> -->
            <a href="{{ route('banks.create') }}"><button type="button" class="btn btn-primary btn-sm"> Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="banks-datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode</th>
                  <th>Bank</th>
                  <th>Action</th>
                </tr>
                </thead>                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
</section>
<!-- /.content -->
@endsection

@section('scripts')
<!-- page script -->
  <script>
    $(function() {
        $('#banks-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('banks.index') !!}',
            columns: [
                { data: 'DT_Row_Index', orderable:false, searchable:false },
                { data: 'code', name: 'code' },
                { data: 'name', name: 'name' },
                { data: 'action', name: 'action', orderable: false, searchable:false }
            ]
        });
    });
  </script>
@endsection

