<div class="box-body">
  <input name="_token" type="hidden" value="{{ csrf_token() }}">
  <div class="form-group has-feedback {{ $errors->has('code') ? ' has-error' : '' }}">
    <label for="code" class="col-sm-2 control-label">Code</label>
    <div class="col-sm-10">
      {{-- <input type="text" name="code" class="form-control" id="code" placeholder="Company Code"> --}}
      {!! Form::text('code', null, ['class'=>'form-control', 'id'=> 'code', 'placeholder'=> 'Bank Code']) !!}
      {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
    </div>
  </div>
  <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-sm-2 control-label">Bank</label>
    <div class="col-sm-10">
      {{-- <input type="text" name="name" class="form-control" id="name" placeholder="Company Name"> --}}
      {!! Form::text('name', null, ['class'=>'form-control', 'id'=> 'name', 'placeholder'=> 'Bank Name']) !!}
      {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    </div>
  </div>
</div>
<!-- /.box-body -->
<div class="box-footer">
  <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
  <button type="submit" class="btn btn-info pull-right">Submit</button>
</div>
<!-- /.box-footer -->