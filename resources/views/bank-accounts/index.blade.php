@extends('base.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Bank Account
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
    <li class="active">Bank Account</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Companies</h3> -->
            <a href="{{ route('bank-accounts.create') }}"><button type="button" class="btn btn-primary btn-sm"> Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="bank-accounts-datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th>Bank</th>
                  <th>Account Number</th>
                  <th>Currency Type</th>
                  <th>Action</th>
                </tr>
                </thead>                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
</section>
<!-- /.content -->
@endsection

@section('scripts')
<!-- page script -->
  <script>
    $(function() {
        $('#bank-accounts-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('bank-accounts.index') !!}',
            columns: [
                { data: 'DT_Row_Index', orderable:false, searchable:false },
                { data: 'name', name: 'name' },
                { data: 'bank.code', name: 'bank.code' },
                { data: 'account_number', name: 'account_number' },
                { data: 'currency_type.code', name: 'currency_type.code' },
                { data: 'action', name: 'action', orderable: false, searchable:false }
            ]
        });
    });
  </script>
@endsection

