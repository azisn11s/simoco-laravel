<div class="box-body">
  <input name="_token" type="hidden" value="{{ csrf_token() }}">
  

  <div class="form-group has-feedback {{ $errors->has('bank_id') ? ' has-error' : '' }}">
    <label for="bank_id" class="col-sm-2 control-label">Bank</label>
    <div class="col-sm-10">
      {!! Form::select('bank_id', [''=>'--Pilih Bank--']+$bankList, null, ['class'=>'form-control']) !!}
      {!! $errors->first('bank_id', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('currency_type_id') ? ' has-error' : '' }}">
    <label for="currency_type_id" class="col-sm-2 control-label">Currency Type</label>
    <div class="col-sm-10">
      {!! Form::select('currency_type_id', [''=>'--Pilih Currency--']+$currencyList, null, ['class'=>'form-control']) !!}
      {!! $errors->first('currency_type_id', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('account_number') ? ' has-error' : '' }}">
    <label for="account_number" class="col-sm-2 control-label">Account Number</label>
    <div class="col-sm-10">
      {!! Form::text('account_number', null, ['class'=>'form-control', 'id'=> 'account_number', 'placeholder'=> 'Account Number']) !!}
      {!! $errors->first('account_number', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      {{-- <input type="text" name="name" class="form-control" id="name" placeholder="Company Name"> --}}
      {!! Form::text('name', null, ['class'=>'form-control', 'id'=> 'name', 'placeholder'=> 'Account Name']) !!}
      {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    </div>
  </div>



</div>
<!-- /.box-body -->
<div class="box-footer">
  <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
  <button type="submit" class="btn btn-info pull-right">Submit</button>
</div>
<!-- /.box-footer -->