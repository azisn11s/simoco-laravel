@extends('base.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employee
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
    <li><a href="{{ route('employees.index') }}"></i> Employee</a></li>
    <li class="active">Add Record</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Employee</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6">
                <!-- Horizontal Form -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ route('employees.store') }}" method="post">
                  @include('employees._form')
                </form>
                <!-- form end -->
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
</section>
<!-- /.content -->
@endsection


