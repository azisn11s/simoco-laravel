@extends('base.admin')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Employees
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master Data</a></li>
    <li class="active">Employees</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Companies</h3> -->
            <a href="{{ route('employees.create') }}"><button type="button" class="btn btn-primary btn-sm"> Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="employees-datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Full Name</th>
                  <th>Code</th>
                  <th>Department</th>
                  <th>Type</th>
                  <th>Email 1</th>
                  <th>Email 2</th>
                  <th>POB</th>
                  <th>DOB</th>
                  <th>Address</th>
                  <th>Start Work Date</th>
                  <th>Action</th>
                </tr>
                </thead>                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
</section>
<!-- /.content -->
@endsection

@section('scripts')
<!-- page script -->
  <script>
    $(function() {
        $('#employees-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('employees.index') !!}',
            scrollX: true,
            columns: [
                { data: 'DT_Row_Index', orderable:false, searchable:false },
                { data: 'first_name', name: 'first_name' },
                { data: 'code', name: 'code' },
                { data: 'department.name', name: 'department.name' },
                { data: 'employee_type.name', name: 'employee_type.name'},
                { data: 'email_1', name: 'email_1'},
                { data: 'email_2', name: 'email_2'},
                { data: 'place_of_birth', name: 'place_of_birth'},
                { data: 'date_of_birth', name: 'date_of_birth'},
                { data: 'address', name: 'address'},
                { data: 'start_work_date', name: 'start_work_date'},
                { data: 'action', name: 'action', orderable: false, searchable:false }
            ]
        });
    });
  </script>
@endsection

