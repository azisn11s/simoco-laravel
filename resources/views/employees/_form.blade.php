<div class="box-body">
  <input name="_token" type="hidden" value="{{ csrf_token() }}">
  
  <div class="form-group has-feedback {{ $errors->has('first_name') ? ' has-error' : '' }}">
    <label for="first_name" class="col-sm-3 control-label">First Name</label>
    <div class="col-sm-9">
      {!! Form::text('first_name', null, ['class'=>'form-control', 'id'=> 'first_name', 'placeholder'=> 'First Name']) !!}
      {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label for="last_name" class="col-sm-3 control-label">Last Name</label>
    <div class="col-sm-9">
      {!! Form::text('last_name', null, ['class'=>'form-control', 'id'=> 'last_name', 'placeholder'=> 'Last Name']) !!}
      {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('code') ? ' has-error' : '' }}">
    <label for="code" class="col-sm-3 control-label">Code</label>
    <div class="col-sm-9">
      {!! Form::text('code', null, ['class'=>'form-control', 'id'=> 'code', 'placeholder'=> 'Employee Code']) !!}
      {!! $errors->first('code', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('department_id') ? ' has-error' : '' }}">
    <label for="department_id" class="col-sm-3 control-label">Department</label>
    <div class="col-sm-9">
      {!! Form::select('department_id', [''=>'--Pilih Department--']+$departmentList, null, ['class'=>'form-control']) !!}
      {!! $errors->first('department_id', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('employee_type_id') ? ' has-error' : '' }}">
    <label for="employee_type_id" class="col-sm-3 control-label">Employee Type</label>
    <div class="col-sm-9">
      {!! Form::select('employee_type_id', [''=>'--Pilih Employee Type--']+$employeeTypeList, null, ['class'=>'form-control']) !!}
      {!! $errors->first('employee_type_id', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('email_1') ? ' has-error' : '' }}">
    <label for="email_1" class="col-sm-3 control-label">Email 1</label>
    <div class="col-sm-9">
      {!! Form::email('email_1', null, ['class'=>'form-control', 'id'=> 'email_1', 'placeholder'=> 'Email (primary)']) !!}
      {!! $errors->first('email_1', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('email_2') ? ' has-error' : '' }}">
    <label for="email_2" class="col-sm-3 control-label">Email 2</label>
    <div class="col-sm-9">
      {!! Form::email('email_2', null, ['class'=>'form-control', 'id'=> 'email_2', 'placeholder'=> 'Email (optional)']) !!}
      {!! $errors->first('email_2', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
    <label for="place_of_birth" class="col-sm-3 control-label">POB</label>
    <div class="col-sm-9">
      {!! Form::text('place_of_birth', null, ['class'=>'form-control', 'id'=> 'place_of_birth', 'placeholder'=> 'Place Of Birth']) !!}
      {!! $errors->first('place_of_birth', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
    <label for="date_of_birth" class="col-sm-3 control-label">DOB</label>
    <div class="col-sm-9">
      {!! Form::text('date_of_birth', null, ['class'=>'form-control', 'id'=> 'date_of_birth', 'placeholder'=> 'Date Of Birth']) !!}
      {!! $errors->first('date_of_birth', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="address" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-9">
      {!! Form::text('address', null, ['class'=>'form-control', 'id'=> 'address', 'placeholder'=> 'Full Address']) !!}
      {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('home_phone') ? ' has-error' : '' }}">
    <label for="home_phone" class="col-sm-3 control-label">Home Phone</label>
    <div class="col-sm-9">
      {!! Form::text('home_phone', null, ['class'=>'form-control', 'id'=> 'home_phone', 'placeholder'=> 'Home Phone Number']) !!}
      {!! $errors->first('home_phone', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
    <label for="mobile_phone" class="col-sm-3 control-label">Mobile Phone</label>
    <div class="col-sm-9">
      {!! Form::text('mobile_phone', null, ['class'=>'form-control', 'id'=> 'mobile_phone', 'placeholder'=> 'Mobile Phone Number']) !!}
      {!! $errors->first('mobile_phone', '<span class="help-block">:message</span>') !!}
    </div>
  </div>

  <div class="form-group has-feedback {{ $errors->has('start_work_date') ? ' has-error' : '' }}">
    <label for="start_work_date" class="col-sm-3 control-label">Start Work</label>
    <div class="col-sm-9">
      {!! Form::text('start_work_date', null, ['class'=>'form-control', 'id'=> 'start_work_date', 'placeholder'=> 'Start Work Date']) !!}
      {!! $errors->first('start_work_date', '<span class="help-block">:message</span>') !!}
    </div>
  </div>



</div>
<!-- /.box-body -->
<div class="box-footer">
  <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
  <button type="submit" class="btn btn-info pull-right">Submit</button>
</div>
<!-- /.box-footer -->