@php
  $current_path_array = explode('/', Request::path());
@endphp

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('images/icons/user-default.png') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="{{{ (Request::is('home') ? 'active' : '') }}}"><a href="#"><i class="fa fa-book "></i> <span>Home</span></a></li>
      <li class="treeview {{{ ($current_path_array[0] === 'master-data' ? 'active' : '' ) }}}">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'companies' ? 'active' : '' ) }}}"><a href="{{ route('companies.index') }}"><i class="fa fa-circle-o"></i> Company</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'employees' ? 'active' : '' ) }}}"><a href="{{ route('employees.index') }}"><i class="fa fa-circle-o"></i> Employee</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'employee-types' ? 'active' : '' ) }}}"><a href="{{ route('employee-types.index') }}"><i class="fa fa-circle-o"></i> Employee Type</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'departments' ? 'active' : '' ) }}}"><a href="{{ route('departments.index') }}"><i class="fa fa-circle-o"></i> Department</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'banks' ? 'active' : '' ) }}}"><a href="{{ route('banks.index') }}"><i class="fa fa-circle-o"></i> Bank</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'currency-types' ? 'active' : '' ) }}}"><a href="{{ route('currency-types.index') }}"><i class="fa fa-circle-o"></i> Currency Type</a></li>
          <li class="{{{ ((!empty($current_path_array[1])) && $current_path_array[1] === 'bank-accounts' ? 'active' : '' ) }}}"><a href="{{ route('bank-accounts.index') }}"><i class="fa fa-circle-o"></i> Bank Account</a></li>
        </ul>
      </li>
      
      
      <li class="header">LABELS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>