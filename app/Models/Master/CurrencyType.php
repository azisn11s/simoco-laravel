<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrencyType extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_currency_type';

	protected $fillable = ['code', 'name', 'country'];

	public static function getDropdownListAll()
	{
		return self::pluck('name', 'id')->all();
	}

}
