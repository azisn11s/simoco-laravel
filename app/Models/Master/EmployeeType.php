<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeType extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_employee_type';

	protected $fillable = ['code', 'name'];


	public function employees()
	{
	   return $this->hasMany(Models\Master\Employee::class);
	}

	public static function getDropdownListAll()
	{
		return self::pluck('name', 'id')->all();
	}

    
}
