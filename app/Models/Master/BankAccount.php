<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_bank_account';

	protected $fillable = [
		'bank_id',
		'name',
		'account_number',
		'currency_type_id',
	];

	public function bank()
	{
	   return $this->belongsTo(Bank::class);
	}

	public function currencyType()
	{
	   return $this->belongsTo(CurrencyType::class);
	}


}
