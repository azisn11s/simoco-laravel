<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];

    protected $table = 'mst_company';

    protected $fillable = ['code', 'name', 'description'];
    
}
