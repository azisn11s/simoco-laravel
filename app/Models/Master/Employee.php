<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_employee';

	protected $fillable = [
		'code',
		'first_name',
		'last_name',
		'department_id',
		'email_1',
		'email_2',
		'employee_type_id',
		'place_of_birth',
		'date_of_birth',
		'address',
		'home_phone',
		'mobile_phone',
		'start_work_date',
		'status'
	];


	public function employeeType()
	{
		return $this->belongsTo(EmployeeType::class);
	}

	public function department()
	{
		return $this->belongsTo(Department::class);
	}

    
}
