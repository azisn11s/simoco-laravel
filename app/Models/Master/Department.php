<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_department';

	protected $fillable = [
		'code', 
		'name',
		'description',
	];


	public function employees()
	{
	   return $this->hasMany(Employee::class);
	}

	public static function getDropdownListAll()
	{
		return self::pluck('name', 'id')->all();
	}

    
}
