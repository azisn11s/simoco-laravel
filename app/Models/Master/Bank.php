<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'mst_bank';

	protected $fillable = ['code', 'name'];

	public static function getDropdownListAll()
	{
		return self::pluck('name', 'id')->all();
	}

    
}
