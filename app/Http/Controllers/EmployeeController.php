<?php

namespace App\Http\Controllers;

use App\Models\Master\Employee;
use App\Models\Master\Department;
use App\Models\Master\EmployeeType;
use Illuminate\Http\Request;

use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $employees = Employee::with('employeeType', 'department')->get();
            
            return Datatables::of($employees)
                ->addIndexColumn()
                ->addColumn('action', function($employee){
                    return view('datatable._action', [
                        'model'           => $employee,
                        'form_url'        => route('employees.destroy', $employee->id),
                        'edit_url'        => route('employees.edit', $employee->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $employee->name . '?'
                    ]);
                })                
                ->make(true);
        }

        return view('employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departmentList = Department::getDropdownListAll();
        $employeeTypeList = EmployeeType::getDropdownListAll();

        return view('employees.create', compact('departmentList', 'employeeTypeList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
