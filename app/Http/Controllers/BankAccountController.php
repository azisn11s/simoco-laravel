<?php

namespace App\Http\Controllers;

use App\Models\Master\BankAccount;
use App\Models\Master\Bank;
use App\Models\Master\CurrencyType;
use Illuminate\Http\Request;

use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $bank_accounts = BankAccount::with('bank','currencyType')->get();
            
            return Datatables::of($bank_accounts)
                ->addIndexColumn()
                ->addColumn('action', function($bank_account){
                    return view('datatable._action', [
                        'model'           => $bank_account,
                        'form_url'        => route('bank-accounts.destroy', $bank_account->id),
                        'edit_url'        => route('bank-accounts.edit', $bank_account->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $bank_account->name . '?'
                    ]);
                })                
                ->make(true);
        }

        return view('bank-accounts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bankList = Bank::getDropdownListAll();
        $currencyList = CurrencyType::getDropdownListAll();

        return view('bank-accounts.create', compact('bankList', 'currencyList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'bank_id'=> 'required|exists:mst_bank,id',
            'currency_type_id' => 'required|exists:mst_currency_type,id',
            'account_number'=> 'required|unique:mst_bank_account',
            'name'=> 'required|unique:mst_bank_account',
        ]);

        $bank_account = BankAccount::create($request->all());

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $bank_account->name"
        ]);

        return redirect()->route('bank-accounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankList = Bank::getDropdownListAll();
        $currencyList = CurrencyType::getDropdownListAll();

        $bank_account = BankAccount::find($id);
        return view('bank-accounts.edit')->with(compact('bank_account','bankList','currencyList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bank_account = BankAccount::find($id);
        $bank_account->update($request->only('bank_id', 'currency_type_id', 'account_number', 'name'));
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $bank_account->name"
        ]);

        return redirect()->route('bank-accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank_account = BankAccount::find($id);
        $bank_account->delete();

        /*if ($member->hasRole('member')) {
            $member->delete();
            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Member berhasil dihapus"
            ]);
        }*/

        return redirect()->route('bank-accounts.index');
    }
}
