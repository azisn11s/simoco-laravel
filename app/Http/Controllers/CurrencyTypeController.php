<?php

namespace App\Http\Controllers;

use App\Models\Master\CurrencyType;
use Illuminate\Http\Request;

use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class CurrencyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $currency_types = CurrencyType::get();
            
            return Datatables::of($currency_types)
                ->addIndexColumn()
                ->addColumn('action', function($currency_type){
                    return view('datatable._action', [
                        'model'           => $currency_type,
                        'form_url'        => route('currency-types.destroy', $currency_type->id),
                        'edit_url'        => route('currency-types.edit', $currency_type->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $currency_type->name . '?'
                    ]);
                })                
                ->make(true);
        }

        return view('currency-types.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('currency-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code'=> 'required|unique:mst_currency_type',
            'name' => 'required',
        ]);
        $currency_type = CurrencyType::create($request->all());
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $currency_type->name"
        ]);
        return redirect()->route('currency-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function show(CurrencyType $currencyType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency_type = CurrencyType::find($id);
        return view('currency-types.edit')->with(compact('currency_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency_type = CurrencyType::find($id);
        $currency_type->update($request->only('code', 'name'));
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $currency_type->name"
        ]);

        return redirect()->route('currency-types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency_type = CurrencyType::find($id);
        $currency_type->delete();

        /*if ($member->hasRole('member')) {
            $member->delete();
            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Member berhasil dihapus"
            ]);
        }*/

        return redirect()->route('currency-types.index');
    }
}
