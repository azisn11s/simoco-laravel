<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master\Company;

use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
		if ($request->ajax()) {
			$companies = Company::get();
			
			return Datatables::of($companies)
				->addIndexColumn()
				->addColumn('action', function($company){
					return view('datatable._action', [
						'model'           => $company,
						'form_url'        => route('companies.destroy', $company->id),
						'edit_url'        => route('companies.edit', $company->id),
						'confirm_message' => 'Yakin mau menghapus ' . $company->name . '?'
					]);
				})                
				->make(true);
		}

		return view('companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        	'code'=> 'required|unique:mst_company',
        	'name' => 'required|unique:mst_company',
        	'description'=> 'max:255'
        ]);
        $company = Company::create($request->all());
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $company->name"
        ]);
        return redirect()->route('companies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('companies.edit')->with(compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->update($request->only('code', 'name', 'description'));
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $company->name"
        ]);

        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     * Using SoftDelete
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();

        /*if ($member->hasRole('member')) {
            $member->delete();
            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Member berhasil dihapus"
            ]);
        }*/

        return redirect()->route('companies.index');
    }
    

}
