<?php

namespace App\Http\Controllers;

use App\Models\Master\Department;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $departments = Department::get();
            
            return Datatables::of($departments)
                ->addIndexColumn()
                ->addColumn('action', function($department){
                    return view('datatable._action', [
                        'model'           => $department,
                        'form_url'        => route('departments.destroy', $department->id),
                        'edit_url'        => route('departments.edit', $department->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $department->name . '?'
                    ]);
                })                
                ->make(true);
        }

        return view('departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code'=> 'required|unique:mst_department',
            'name' => 'required|unique:mst_department',
            'description'=> 'max:255'
        ]);
        $department = Department::create($request->all());
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $department->name"
        ]);

        return redirect()->route('departments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        return view('departments.edit')->with(compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        $department->update($request->only('code', 'name', 'description'));
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $department->name"
        ]);

        return redirect()->route('departments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        $department->delete();

        /*if ($member->hasRole('member')) {
            $member->delete();
            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Member berhasil dihapus"
            ]);
        }*/

        return redirect()->route('departments.index');
    }
}
