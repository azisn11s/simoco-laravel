<?php

namespace App\Http\Controllers;

use App\Models\Master\EmployeeType;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;

class EmployeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $employee_types = EmployeeType::get();
            
            return Datatables::of($employee_types)
                ->addIndexColumn()
                ->addColumn('action', function($employee_type){
                    return view('datatable._action', [
                        'model'           => $employee_type,
                        'form_url'        => route('employee-types.destroy', $employee_type->id),
                        'edit_url'        => route('employee-types.edit', $employee_type->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $employee_type->name . '?'
                    ]);
                })                
                ->make(true);
        }

        return view('employee-types.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code'=> 'required|unique:mst_employee_type',
            'name' => 'required|unique:mst_employee_type'
        ]);
        $employee_type = EmployeeType::create($request->all());
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $employee_type->name"
        ]);

        return redirect()->route('employee-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeType $employeeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee_type = EmployeeType::find($id);
        return view('employee-types.edit')->with(compact('employee_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee_type = EmployeeType::find($id);
        $employee_type->update($request->only('code', 'name'));
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan $employee_type->name"
        ]);

        return redirect()->route('employee-types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\EmployeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee_type = EmployeeType::find($id);
        $employee_type->delete();

        /*if ($member->hasRole('member')) {
            $member->delete();
            Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Member berhasil dihapus"
            ]);
        }*/

        return redirect()->route('employee-types.index');
    }
}
