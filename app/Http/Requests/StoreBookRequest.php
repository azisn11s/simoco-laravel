<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|unique:books,title',
            'author_id' => 'required|exists:authors,id',
            'category_id' => 'required|exists:categories,id',
            'publisher_id' => 'required|exists:publishers,id',
            'amount'    => 'numeric',
            'cover'     => 'image|max:2048',
            'pdf_file' => 'mimes:pdf',
            'total_page'=> 'numeric',
            'publish_place'=>'max:70',
            'publish_date'=>'date',
            'description'=> 'string'
        ];
    }
}
